# Catalogs Reader

Combination of e-book catalogs reader and torrent client search engine in one application, which allows you to keep all subscription and proxy settings in one please. Catalogs can be found here:

* [Torrent Search Engines](https://axet.gitlab.io/android-torrent-client/)
* [Books Catalogs](https://axet.gitlab.io/android-book-reader/)

# Manual install

    gradle installDebug

# Translate

If you want to translate 'Catalogs Reader' to your language  please read following:

  * [HOWTO-Translate.md](/docs/HOWTO-Translate.md)

# Screenshots

![shot](/docs/shot.png)