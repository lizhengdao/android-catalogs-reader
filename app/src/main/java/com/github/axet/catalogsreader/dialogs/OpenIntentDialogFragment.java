package com.github.axet.catalogsreader.dialogs;

import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.github.axet.androidlibrary.widgets.DialogFragmentCompat;
import com.github.axet.androidlibrary.widgets.ErrorDialog;
import com.github.axet.catalogsreader.activities.MainActivity;
import com.github.axet.catalogsreader.app.CatalogsApplication;
import com.github.axet.catalogsreader.navigators.Search;
import com.github.axet.catalogsreader.widgets.Drawer;

public class OpenIntentDialogFragment extends DialogFragmentCompat {

    Handler handler = new Handler();
    Thread t;

    @Override
    public void onStart() {
        super.onStart();

        final MainActivity main = (MainActivity) getActivity();

        t = new Thread("Delayed Intent") {
            @Override
            public void run() {
                try {
                    openURL(getArguments().getString("url"));
                } catch (final RuntimeException e) {
                    ErrorDialog.Post(main, e);
                }
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (main.isFinishing())
                            return;
                        dismissAllowingStateLoss(); //  Can not perform this action after onSaveInstanceState
                    }
                });
            }
        };
        t.start();
    }

    public void openURL(final String str) {
        final MainActivity main = (MainActivity) getActivity();
        if (main == null || main.isFinishing()) // when app was destoryed
            return;
        if (str.startsWith(CatalogsApplication.SCHEME_MAGNET)) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    if (main.isFinishing())
                        return;
                    main.getEngines().addManget(str);
                }
            });
            return;
        }
        if (str.startsWith(Drawer.SCHEME_CATALOG)) {
            main.loadCatalog(Uri.parse(str), null);
            return;
        }
    }

    @Override
    public View createView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ProgressBar view = new ProgressBar(inflater.getContext());
        view.setIndeterminate(true);

        // wait until torrent loaded
        setCancelable(false);

        //getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return view;
    }
}
